Berlatih SQL

1. Membuat database
   
   create database myshop

2. Membuat Table dalam database
-  [table user]
    create table users(
    -> id int AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255));

-  [table categories]
    create table categories(
    -> id int AUTO_INCREMENT PRIMARY KEY,
    -> name varchar (255));

-  [table items]
    create table items(
    -> id int AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> FOREIGN KEY (category_id) REFERENCES categories(id));

3. Memasukkan data pada database
-  [users]
    insert into users(name, email, password)
    -> values ("John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123");

-  [categories]
    insert into categories(name)
    -> values ("gadget"),("cloth"),("men"),("women"),("branded");

-  [items]
    insert into items (name, description, price, stock, category_id)
    -> values ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil data dari database
   a. Mengambil data dari users
   
   SELECT id, name, email FROM users;

   b. Mengambil data items

   SELECT * FROM items WHERE price >= 1000000;
   SELECT * FROM items WHERE name LIKE '%watch';

   c. Menampilkan data items join dengan kategori
   SELECT * FROM items INNER JOIN categories ON items.category_id=categories.id;

5. Mengubah data dari database

   UPDATE items SET price = 25000000 WHERE name = "Sumsang b50";